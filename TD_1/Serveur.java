import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

import object.Point2D;
import object.Restaurant;


public class Serveur {
    public static void main (String[] args) {

        Restaurant[] restaurants = createRestaurantArray();

        try {
            ServerSocket conn = new ServerSocket(12345);
            System.out.println("Wating for a conexion");
            Socket comm = conn.accept();
            ObjectOutputStream outs = new ObjectOutputStream(comm.getOutputStream());
            ObjectInputStream in = new ObjectInputStream(comm.getInputStream());

           Point2D positionClient = (Point2D) in.readObject();
            System.out.println("Connected");
           Restaurant[] nearestRestau = nearestRestaurant(restaurants, positionClient);
           outs.writeObject(nearestRestau);
            System.out.println("Restaurant sended");




            conn.close();
        }catch (IOException IOE){
            System.out.println(IOE.getMessage());
        }catch (ClassNotFoundException CNF){
            System.out.println(CNF.getMessage());
        }
    }

    public static Restaurant[] nearestRestaurant(Restaurant[] restaurants, Point2D position){
        Restaurant[] nearestRestau = {restaurants[0], restaurants[1], restaurants[3]};
        nearestRestau[2]=restaurants[0];
        for (Restaurant restaurant: restaurants) {
            if (restaurant.isNearer(nearestRestau[2],position)) {
                if (restaurant.isNearer(nearestRestau[1],position) )
                {
                    if (restaurant.isNearer(nearestRestau[1],position) )
                    {
                        if (restaurant.isNearer(nearestRestau[0],position))
                        {
                            nearestRestau[2] = nearestRestau[1];
                            nearestRestau[1] = nearestRestau[0];
                            nearestRestau[0] = restaurant;
                        }
                    }else {
                        nearestRestau[2] = nearestRestau[1];
                        nearestRestau[1] = restaurant;
                    }

                } else nearestRestau[2] = restaurant;

            }
        }
        return nearestRestau;


    }

    private static Restaurant[] createRestaurantArray() {
        return new Restaurant[]{
                new Restaurant(new Point2D(4, 1),"Pic", "01 45 23 12 14"),
                new Restaurant( new Point2D(3, 8),"Mcdo", "06 63 93 52 10"),
                new Restaurant( new Point2D(15, 3), "KFC", "06 41 22 63 89"),
                new Restaurant(new Point2D(4, -1), "Sun burger", "09 47 87 36 52"),
                new Restaurant(new Point2D(1, 0), "Pizza Time", "07 61 87 48 72"),
                new Restaurant(new Point2D(-2, 4), "O'tacos", "07 64 41 28 78"),
                new Restaurant(new Point2D(5, 6), "Burger King", "01 45 36 58 98"),
                new Restaurant(new Point2D(2, -2), "Phil", "07 54 63 89 45"),

        };
    }

}
