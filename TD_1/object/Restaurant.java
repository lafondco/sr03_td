package object;

import java.io.Serializable;

public class Restaurant implements  Serializable{
    private String name;
    private String number;
    private Point2D position;

    public Restaurant (Point2D position, String name, String number){
        this.position=position;
        this.name=name;
        this.number=number;
    }

    public String getName() {return name;}
    public String getNumber() {return number;}
    public Point2D getPosition() {return position;}


    public boolean isNearer(Restaurant restaurant, Point2D clientPosition) {
        return this.position.calculerDistance(clientPosition) < restaurant.getPosition().calculerDistance(clientPosition);
    }

    public String toString() {
        return
                "Nom du restaurant : " + name + "\n" +
                        "Numéro de téléphone : " + number + "\n" +
                        "Position : " + position.getX() + position.getY();
    }

}
