package object;

import java.io.Serializable;


public class Point2D implements  Serializable{
    private float x;
    private float y;

    public Point2D (float x, float y){
        this.x=x;
        this.y=y;
    }

    public float getX () {return x;}
    public float getY () {return y;}


    public double calculerDistance(Point2D A){
        return Math.sqrt(Math.pow(A.getX()+this.x, 2)+Math.pow(A.getY()+this.y, 2));
    }




}
