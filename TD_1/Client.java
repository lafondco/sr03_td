import java.io.*;
import java.net.Socket;
import java.util.Scanner;
import java.net.*;


import object.Point2D;
import object.Restaurant;

public class Client {
    public static void main (String[] args)
    {
        try{
            Socket comm = new Socket("localhost", 12345);
            ObjectOutputStream outs = new ObjectOutputStream(comm.getOutputStream());
            ObjectInputStream in = new ObjectInputStream(comm.getInputStream());

            Scanner scanner= new Scanner(System.in);
            System.out.println("Entre votre position : \nx=");
            int x=scanner.nextInt();
            System.out.println("y=");
            int y=scanner.nextInt();
            Point2D positionClient= new Point2D(x,y);

            outs.writeObject(positionClient);

            Restaurant[] nearestRestau = (Restaurant[]) in.readObject();

            System.out.println("Plus proche : \n");
            System.out.println(nearestRestau[0].toString());
            System.out.println("Deuxième lus proche : \n");
            System.out.println(nearestRestau[1].toString());
            System.out.println("Troisième plus proche : \n");
            System.out.println(nearestRestau[2].toString());


        }
        catch (IOException IOE){
            System.out.println(IOE.getMessage());
        }catch (ClassNotFoundException CNF){
            System.out.println(CNF.getMessage());
        }

    }
}
